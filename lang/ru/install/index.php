<?
$MESS["TBWP_CART_SHARING_MODULE_NAME"] = "TBWP Поделиться корзиной";
$MESS["TBWP_CART_SHARING_MODULE_DESCRIPTION"] = "Модуль дает возможность поделиться корзиной пользователя";
$MESS['TBWP_CART_SHARING_PARTNER_NAME'] = 'TheBestWebPro';

$MESS['TBWP_CART_SHARING_INSTALL_ERROR_VERSION'] = 'Версия главного модуля ниже 14. Не поддерживается технология D7. Пожалуйста обновите систему.';
$MESS['TBWP_CART_SHARING_INSTALL_ERROR_NEED_MODULES'] = 'Для установки данного решения необходимо наличие модуля #MODULE#.';
?>