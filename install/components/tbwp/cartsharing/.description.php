<?
use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
Loc::loadMessages(__FILE__);
$arComponentDescription = array(
	"NAME" => Loc::getMessage("TBWP_CART_SHARING_MAIN_COMPONENT_NAME"),
	"DESCRIPTION" => Loc::getMessage("TBWP_CART_SHARING_MAIN_COMPONENT_DESCRIPTION"),
	"SORT" => 500,
	"PATH" => array(
		"ID" => "tbwp",
		"SORT" => 500,
		"NAME" => Loc::getMessage("TBWP_COMPONENTS_FOLDER_NAME"),
	),
);

?>