<?php
$MESS["TBWP_CART_SHARING_MODULE_NOT_INSTALL"] = "Модуль \"#MODULE#\" не установлен";
$MESS["TBWP_CART_SHARING_ERROR_EMPTY_CART"] = "Ваша корзина пуста. Мы не можем создать ссылку для нее.";
$MESS["TBWP_CART_SHARING_ERROR_ADD"] = "Ошибка добавления данных в таблицу tbwp_cart_sharing";

