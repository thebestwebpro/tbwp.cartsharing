<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;
/**
 * @var string $componentPath
 * @var string $componentName
 * @var array $arCurrentValues
 * @global CUserTypeManager $USER_FIELD_MANAGER
 */

if (!\Bitrix\Main\Loader::includeModule('tbwp.cartsharing'))
    return false;

$arUserGroups=[];
$rsUserGroups = \Bitrix\Main\GroupTable::getList(array(
    'select'  => array('NAME','ID','STRING_ID','C_SORT'),
));

while ($arGroup = $rsUserGroups->fetch()) {
    $arUserGroups[$arGroup['ID']]=$arGroup['NAME'];
}

CBitrixComponent::includeComponentClass($componentName);
$arComponentParameters = array(
    "PARAMETERS" => array(
        "USERS_GROUP"=>array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("TBWP_CART_SHARING_F_USERS_GROUP"),
            "TYPE" => "LIST",
            "VALUES" => $arUserGroups,
            "MULTIPLE"=>"Y"
        ),
        "LOAD_JS_LIB" => array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("TBWP_CART_SHARING_F_LOAD_JS_LIB"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N"
        ),
        "SHORT_LINK" => array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("TBWP_CART_SHARING_F_SHORT_LINK"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N"
        ),
        "BASKET_URL" => array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("TBWP_CART_SHARING_F_BASKET_URL"),
            "TYPE" => "STRING",
            "DEFAULT" => "/personal/cart/"
        ),
        "URL_PARAMS" => array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("TBWP_CART_SHARING_F_URL_PARAMS"),
            "TYPE" => "STRING",
            "DEFAULT" => 'share_cart',
        ),
    ),
);
