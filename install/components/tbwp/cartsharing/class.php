<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use Bitrix\Main\Error;
use Bitrix\Main\Errorable;
use Bitrix\Main\ErrorCollection;
use Bitrix\Main\Engine\ActionFilter;
use Bitrix\Main\Engine\Contract\Controllerable;
use Bitrix\Sale;
use Bitrix\Main\Web\Json;
use Tbwp\CartSharing\CartSharingTable;


class TbwpCartSharingComponent extends CBitrixComponent implements Controllerable, Errorable
{

    protected ErrorCollection $errorCollection;

    public function onIncludeComponentLang()
    {
        Loc::loadMessages(__FILE__);
    }

    public function getErrors(): array
    {
        return $this->errorCollection->toArray();
    }

    public function getErrorByCode($code): Error
    {
        return $this->errorCollection->getErrorByCode($code);
    }

    protected function showErrors() :void
    {
        foreach ($this->getErrors() as $error)
        {
            ShowError($error);
        }
    }

    public function configureActions(): array
    {
        return [
            'generateLink' => [
                'prefilters' => [
                    new ActionFilter\Csrf(),
                ]
            ]
        ];
    }

    /**
     * @return array
     */
    protected function listKeysSignedParameters(): array
    {
        return array(
            "BASKET_URL",
            "URL_PARAM",
            "SHORT_LINK",
        );
    }

    /**
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams): array
    {
        $arParams = parent::onPrepareComponentParams($arParams);

        $this->errorCollection = new ErrorCollection();

        $arParams["BASKET_URL"] = trim($arParams["BASKET_URL"]);
        $arParams["LOAD_JS_LIB"] = $arParams["LOAD_JS_LIB"]=="Y";
        $arParams["SHORT_LINK"] = $arParams["SHORT_LINK"]=="Y";

        $arParams["URL_PARAM"] = (isset($arParams["URL_PARAM"]) ? (string)$arParams["URL_PARAM"] : '');
        if(
            $arParams["URL_PARAM"] == ''
            || !preg_match("/^[A-Za-z_]*$/", $arParams["FILTER_SECTION_NAME"])
        )
        {
            $arParams["URL_PARAM"] = "sharing_cart";
        }

        return $arParams;
    }

    /**
     * @return false|void
     */
    public function executeComponent()
    {
        try
        {
            $this->checkModules();

            if($this->arParams['LOAD_JS_LIB'])
                \CJSCore::Init(['Fancybox3']);

            if(!empty($this->arParams['USERS_GROUP'])){
                $arUserGroups=\Bitrix\Main\Engine\CurrentUser::get()->getUserGroups();
                if(empty(array_intersect($this->arParams['USERS_GROUP'],$arUserGroups)))
                    return false;
            }

            if(!empty($this->request->get($this->arParams['URL_PARAM']))){
                $this->processAdd($this->request->get($this->arParams['URL_PARAM']));
            }

            $this->includeComponentTemplate();
        }
        catch (\Exception $e)
        {
            $this->errorCollection->add([new Error($e->getMessage())]);
            $this->showErrors();
        }

    }

    /**
     * @return void
     * @throws \Bitrix\Main\LoaderException
     */
    protected function checkModules() :void
    {
        if (!Loader::includeModule('sale'))
            throw new Exception(Loc::getMessage('TBWP_CART_SHARING_MODULE_NOT_INSTALL',["#MODULE#"=>'sale']));

        if (!Loader::includeModule('highloadblock'))
            throw new Exception(Loc::getMessage('TBWP_CART_SHARING_MODULE_NOT_INSTALL',["#MODULE#"=>'highloadblock']));

        if (!Loader::includeModule('tbwp.cartsharing'))
            throw new Exception(Loc::getMessage('TBWP_CART_SHARING_MODULE_NOT_INSTALL',["#MODULE#"=>'tbwp.cartsharing']));
    }

    /**
     * @param $hashString
     * @return false|void
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     * @throws \Bitrix\Main\ArgumentTypeException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\NotSupportedException
     * @throws \Bitrix\Main\ObjectNotFoundException
     */
    protected function processAdd(string $hashString)
    {
        $arItem=$this->getItemByHash($hashString);
        if(empty($arItem))
            return false;

        $arProducts=Json::decode($arItem['VALUE']);

        $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), \Bitrix\Main\Context::getCurrent()->getSite());
        $basket->clearCollection();

        foreach ($arProducts as $productId=>$productItem){
            /** @var Sale\BasketItem $item */

            $item = $basket->createItem('catalog', $productId);
            $item->setFields(array(
                'QUANTITY' => $productItem['QUANTITY'],
                'CURRENCY' => Bitrix\Currency\CurrencyManager::getBaseCurrency(),
                'LID' => Bitrix\Main\Context::getCurrent()->getSite(),
                'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
            ));

            $basket->save();
            if(empty($productItem['PROPERTIES']))
                continue;

            $basketPropertyCollection = $item->getPropertyCollection();
            foreach ($productItem['PROPERTIES'] as $PROPERTY){
                unset($PROPERTY['ID']);
                $item = $basketPropertyCollection->createItem();
                $item->setFields($PROPERTY);
            }
            $basketPropertyCollection->save();
        }

        $session = \Bitrix\Main\Application::getInstance()->getSession();
        $session->set('cartsharing_redirect', 'Y');

        $uri=new \Bitrix\Main\Web\Uri($this->request->getRequestUri());
        $uri->deleteParams([$this->arParams['URL_PARAM']]);
        $uri->addParams(['cartsharing_redirect'=>'Y']);
        LocalRedirect($uri->getUri());

    }


    /**
     * @return string
     */
    protected function getServerUrl():string
    {
        $server=\Bitrix\Main\Application::getInstance()->getContext()->getServer();
        return ($this->request->isHttps()) ? "https://".$server->getServerName() : "http://".$server->getServerName();
    }

    /**
     * @param array $params
     * @return string
     */
    protected function makeUrl(array $params)
    {
        $uri =new \Bitrix\Main\Web\Uri($this->getServerUrl());
        $uri->setPath($this->arParams['BASKET_URL']);
        $uri->addParams($params);
        return $uri->getUri();
    }

    /**
     * @param string $url
     * @return string
     * @throws Exception
     */
    protected function getShortUrl(string $url):string
    {

        //делаем ссылку не абсолютной, а относительной
        $url=str_replace($this->getServerUrl(),"",$url);

        $rsData = \CBXShortUri::GetList(Array(),Array("URI_EXACT" => $url));
        if(!$arRes = $rsData->Fetch()) {
            $str_SHORT_URI = \CBXShortUri::GenerateShortUri();
            $arRes = Array(
                "URI" => $url,
                "SHORT_URI" => $str_SHORT_URI,
                "STATUS" => "301",
            );
            $ID = \CBXShortUri::Add($arRes);
            if(!$ID){
                throw new Exception(implode(". ",\CBXShortUri::GetErrors()));
            }
        }
        $uri =new \Bitrix\Main\Web\Uri($this->getServerUrl());
        $uri->setPath("/".$arRes["SHORT_URI"]);

        return $uri->getUri();
    }

    /**
     * @param string $hash
     * @return mixed
     */
    protected function getItemByHash(string $hash):mixed
    {
        return CartSharingTable::getList(['filter'=>['=HASH'=>$hash]])->fetch();
    }

    /**
     * @param array $arFields
     * @return string
     * @throws \Bitrix\Main\ArgumentException|\Bitrix\Main\SystemException
     */
    protected function makeItemHash(array $arFields):string
    {
        $value=Json::encode($arFields);
        $hash=hash('crc32', $value, FALSE);
        $rsItem=CartSharingTable::getList(['filter'=>['=HASH'=>$hash]]);
        if(!$arItem=$rsItem->fetch()){
            $arItem=[
                "HASH"=>$hash,
                "VALUE"=>$value,
            ];
            $result=CartSharingTable::add($arItem);
            if(!$result->isSuccess())
                throw new Exception(Loc::getMessage('TBWP_CART_SHARING_ERROR_ADD'));
        }

        return $arItem["HASH"];
    }

    /**
     * @return array
     */
    public function generateLinkAction():array
    {
        $arResult=[];
        try {
            $this->checkModules();
            $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), \Bitrix\Main\Context::getCurrent()->getSite());
            $basketItems = $basket->getBasketItems();
            if(empty($basketItems))
                throw new Exception(Loc::getMessage('TBWP_CART_SHARING_ERROR_EMPTY_CART'));

            $arProductItems=[];
            /** @var \Bitrix\Sale\BasketItem $basketItem */
            foreach ($basket as $basketItem) {
                $arProductItems[$basketItem->getProductId()]['QUANTITY']=$basketItem->getQuantity();
                $basketPropertyCollection = $basketItem->getPropertyCollection();
                if(!$basketPropertyCollection)
                    continue;

                $arProductItems[$basketItem->getProductId()]['PROPERTIES']=$basketPropertyCollection->getPropertyValues();
                unset($arProductItems[$basketItem->getProductId()]['PROPERTIES']['CATALOG.XML_ID']);
                unset($arProductItems[$basketItem->getProductId()]['PROPERTIES']['PRODUCT.XML_ID']);
                foreach ($arProductItems[$basketItem->getProductId()]['PROPERTIES'] as $key=>$prop){
                    unset($arProductItems[$basketItem->getProductId()]['PROPERTIES'][$key]['ID']);
                    unset($arProductItems[$basketItem->getProductId()]['PROPERTIES'][$key]['SORT']);
                }

                if(empty($arProductItems[$basketItem->getProductId()]['PROPERTIES']))
                    unset($arProductItems[$basketItem->getProductId()]['PROPERTIES']);
            }

            $linkHash=$this->makeItemHash($arProductItems);
            $arResult['URL']=$this->makeUrl([$this->arParams['URL_PARAM']=>$linkHash]);
            if($this->arParams['SHORT_LINK'])
                $arResult['URL']=$this->getShortUrl($arResult['URL']);

        }catch (\Exception $e){
            $arResult['ERROR']=$e->getMessage();
        }

        return $arResult;
    }

}