class TbwpCartSharing{

    constructor (arParams)
    {
        this.componentName = arParams.componentName;
        this.signedParameters = arParams.signedParameters;
        this.modalId = arParams.modalId;
        this.btn_copy_message=arParams.btn_copy_message
        this.btn_copy_success_message=arParams.btn_copy_success_message
    }

    shareClick()
    {
        this.sendRequest();
    }

    sendRequest()
    {
        BX.showWait();
        BX.ajax.runComponentAction(this.componentName, 'generateLink', {
            mode: 'class',
            signedParameters: this.signedParameters,
        }).then((response)=> {
            BX.closeWait();
            this.processResult(response);
        }).catch((err)=>{
            console.log(err);
            BX.closeWait();
        });
    }

    processResult(response)
    {
        if(response.status!=="success" || response.data.ERROR)
            return false;
        let modalNode=document.querySelector('#'+this.modalId);
        if(!modalNode)
            return false;
        let inputLink=modalNode.querySelector('[data-entity="link-input"]');
        if(!inputLink)
            return false;

        inputLink.value=response.data.URL;
        let btnCopy=modalNode.querySelector('[data-entity="btn-copy"]');
        btnCopy.innerHTML=this.btn_copy_message;

        this.onShowModal();
    }
    onShowModal()
    {
        if (typeof Fancybox !=="undefined" && Fancybox.getInstance())
        {
            Fancybox.close(true);
        }
        let options={
            dragToClose:false,
            mainClass:'tbwp-cart-sharing',
            on: {
                click:(event) => {
                    return false;
                }
            }
        };
        Fancybox.show([
            {
                src: '#'+this.modalId,
                type: "inline",
            },
        ],options);
    }
    copyLink()
    {
        let modalNode=document.querySelector('#'+this.modalId);
        if(!modalNode)
            return false;
        let inputLink=modalNode.querySelector('[data-entity="link-input"]');
        if(!inputLink)
            return false;

        let btnCopy=modalNode.querySelector('[data-entity="btn-copy"]');

        navigator.clipboard.writeText(inputLink.value).then(() => {
            btnCopy.innerHTML=this.btn_copy_success_message;
        },() => {
            console.error('Failed to copy');
        });
    }
};

