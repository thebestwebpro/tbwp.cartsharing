<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main;
use Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

if(method_exists($this, 'setFrameMode'))
    $this->setFrameMode(true);

$mainId='tbwp_cart_sharing'.$this->randString();
$modalId='tbwp_cart_sharing_modal'.$this->randString();

$arJsParams=[
    'signedParameters'=>$this->getComponent()->getSignedParameters(),
    'componentName'=>$this->getComponent()->getName(),
    'modalId'=>$modalId,
    'btn_copy_message'=>"Скопировать",
    'btn_copy_success_message'=>"Скопировано",
];

?>
<div class="tbwp-cart-sharing" id="<?=$mainId?>">
    <div id="<?=$modalId?>" style="display:none">
        <div class="tbwp-cart-sharing-modal-wrap">
            <div class="h3">Поделиться корзиной</div>
            <p>Скопируйте ссылку, чтобы поделиться ей.</p>
            <div class="row g-3">
                <div class="col-12 col-md-auto">
                    <input type="text" class="form-control" disabled data-entity="link-input">
                </div>
                <div class="col-12 col-md-auto">
                    <a href="javascript:void(0)" onclick="window.TbwpCartSharing.copyLink()" data-entity="btn-copy" class="btn btn-primary">Скопировать</a>
                </div>
            </div>
        </div>

    </div>
</div>
<script>
    BX.ready(function(){
       window.TbwpCartSharing = new TbwpCartSharing(<?=\CUtil::PhpToJSObject($arJsParams)?>);
    });
</script>