<?php
$MESS["TBWP_FAVOURITE_POPUP_TITLE_ADD"] = "Товар добавлен в избранное";
$MESS["TBWP_FAVOURITE_POPUP_TITLE_DELETE"] = "Товар удален из избранного";
$MESS["TBWP_FAVOURITE_POPUP_TITLE_CLEAR"] = "Избранное";
$MESS["TBWP_FAVOURITE_POPUP_TITLE_ERROR"] = "Ошибка";
$MESS["TBWP_FAVOURITE_POPUP_BTN_REDIRECT"] = "Перейти в избранное";
$MESS["TBWP_FAVOURITE_POPUP_BTN_CLOSE"] = "Продолжить";
$MESS["TBWP_FAVOURITE_POPUP_MESS_ADD"] = "Выбранный Вами товар, успешно добавлен в список избранное";
$MESS["TBWP_FAVOURITE_POPUP_MESS_DELETE"] = "Выбранный Вами товар, успешно удален из списка избранное";
$MESS["TBWP_FAVOURITE_POPUP_MESS_CLEAR"] = "Ваш список избранного, был успешно очищен";
$MESS["TBWP_FAVOURITE_ITEM_TITLE_ADD"] = "Добавить в избранное";
$MESS["TBWP_FAVOURITE_ITEM_TITLE_DELETE"] = "Удалить из избранного";