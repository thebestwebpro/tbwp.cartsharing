<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arParams
 * @var array $arResult
 */

$request= \Bitrix\Main\Application::getInstance()->getContext()->getRequest();

$session = \Bitrix\Main\Application::getInstance()->getSession();
if($request->get('cartsharing_redirect')=="Y"){
    ?>
    <script>
        try {
            ym(23682442,'reachGoal','sharing_cart');
        } catch(e) {
            document.addEventListener('yaCounter23682442inited',function(event){
                ym(23682442,'reachGoal','sharing_cart');
            });
        }
    </script>
    <?php
}
