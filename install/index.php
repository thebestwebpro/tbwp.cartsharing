<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Application;

Loc::loadMessages(__FILE__);

if(class_exists("tbwp_cartsharing")) return;

Class tbwp_cartsharing extends \CModule{

    function __construct(){
        $arModuleVersion = array();
        include(dirname(__FILE__)."/version.php");

        $this->MODULE_ID = 'tbwp.cartsharing';
        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->MODULE_NAME = Loc::getMessage("TBWP_CART_SHARING_MODULE_NAME");
        $this->MODULE_DESCRIPTION = Loc::getMessage("TBWP_CART_SHARING_MODULE_DESCRIPTION");

        $this->PARTNER_NAME = Loc::getMessage("TBWP_CART_SHARING_PARTNER_NAME");
    }

    function InstallFiles($arParams = array()){
        CopyDirFiles($this->GetPath()."/install/components", $_SERVER["DOCUMENT_ROOT"]."/bitrix/components/", true, true);
        return true;
    }

    function UnInstallFiles(){
        DeleteDirFiles( $this->GetPath().'/install/components', $_SERVER['DOCUMENT_ROOT'].'/bitrix/components' );
        return true;
    }
    function InstallDB()
    {
        global $DB;
        $DB->runSQLBatch($this->GetPath(). '/install/db/' . mb_strtolower($DB->type) . '/install.sql');
    }

    function UnInstallDB($arParams = Array())
    {
        global $DB;
        $DB->runSQLBatch($this->GetPath() . '/install/db/' . mb_strtolower($DB->type) . '/uninstall.sql');
    }

    function isVersionD7(){
        return CheckVersion(\Bitrix\Main\ModuleManager::getVersion('main'), '14.00.00');
    }

    function GetPath($notDocumentRoot = false){
        if ($notDocumentRoot){
            return str_ireplace(Application::getDocumentRoot(), '', dirname(__DIR__));
        }else{
            return dirname(__DIR__);
        }
    }
    function DoInstall(){

        global $APPLICATION;

        try{
            if (!$this->isVersionD7())
                throw new Exception(Loc::getMessage("TBWP_CART_SHARING_INSTALL_ERROR_VERSION"));

            if (!Loader::includeModule("sale"))
                throw new Exception(Loc::getMessage("TBWP_CART_SHARING_INSTALL_ERROR_NEED_MODULES",['MODULE'=>'Sale']));

            $this->InstallDB();
            $this->InstallFiles();
            \Bitrix\Main\ModuleManager::registerModule($this->MODULE_ID);

        }catch (\Exception $e){
            \Bitrix\Main\ModuleManager::unRegisterModule($this->MODULE_ID);
            $APPLICATION->ThrowException($e->getMessage());
        }
    }

    function DoUninstall()
    {
        global $APPLICATION;

        $this->UnInstallDB();
        $this->UnInstallFiles();
        \Bitrix\Main\Config\Option::delete($this->MODULE_ID);
        \Bitrix\Main\ModuleManager::unRegisterModule($this->MODULE_ID);

        $APPLICATION->IncludeAdminFile(Loc::getMessage("INSTALL_UNINSTALL"), $this->GetPath() . "/install/unstep.php");

    }

}