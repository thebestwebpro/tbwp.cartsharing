<?php
namespace Tbwp\CartSharing;

use Bitrix\Main\Loader;
use Bitrix\Iblock;
use Bitrix\Sale;
use Bitrix\Main\Localization\Loc;

Loader::registerAutoLoadClasses(
    'tbwp.cartsharing',
    array(
        __NAMESPACE__.'\\CartSharingTable' => 'lib/cartsharingtable.php',
    )
);

if(!\CJSCore::IsExtRegistered("Fancybox3")){
    \CJSCore::RegisterExt("Fancybox3", Array(
        'js' => Helper::GetPathModule().'/js/fancybox/fancybox.umd.js',
        'css' => Helper::GetPathModule().'/js/fancybox/fancybox.css',
        'use' => \CJSCore::USE_ADMIN|\CJSCore::USE_PUBLIC
    ));
}

class Helper
{
    const MODULE_ID = 'tbwp.cartsharing';

    public static function GetPathModule($absolute = false)
    {
        $path = str_replace(\Bitrix\Main\Application::getDocumentRoot(), "", __DIR__);
        $path = str_replace(str_replace("/", "\\", \Bitrix\Main\Application::getDocumentRoot()), "", $path);
        $localPath = substr($path, 0, strrpos($path, self::MODULE_ID)) . self::MODULE_ID;
        return $absolute ? \Bitrix\Main\IO\Path::normalize(\Bitrix\Main\Application::getDocumentRoot() . $localPath) : \Bitrix\Main\IO\Path::normalize($localPath);
    }
}