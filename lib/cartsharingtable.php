<?php
namespace Tbwp\CartSharing;

use Bitrix\Main,
    Bitrix\Main\Entity,
    Bitrix\Main\ORM\Fields;

class CartSharingTable extends Main\ORM\Data\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName(): string
    {
        return 'tbwp_cart_sharing';
    }

    /**
     * @return array
     * @throws Main\SystemException
     */
    public static function getMap(): array
    {
        return array(
            new Fields\IntegerField(
                'ID',
                [
                    'primary'      => true,
                    'autocomplete' => true,
                ]
            ),
            new Fields\DatetimeField("DATE_CREATE"),
            new Fields\StringField("HASH",["required"=>true]),
            new Fields\TextField("VALUE",["required"=>true])
        );
    }

    /**
     * @param Entity\Event $event
     * @return Entity\EventResult
     */
    public static function onBeforeAdd(Entity\Event $event): Entity\EventResult
    {
        $result = new Entity\EventResult();

        $result->modifyFields(array(
                'DATE_CREATE' => new Main\Type\DateTime(),
            )
        );

        return $result;
    }

}